const express = require('express');
const app = express();
const port = 5000;
app.get('/', (req, res) => res.send('Hello World!'));

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`));
//
// import appLoader from './loaders';
// import config from './config/index';
//
// /**
//  * @desc Визначаємо функцію для запуска сервера
//  **/
// async function startServer() {
//     const app = express();
//
//     // Ініціалізуємо та запускаємо лоадер для нашого сервака
//     // Таким чином ми повиносили з index.js багато зайвого, "розбивши" процесс
//     // запуску та обробки вхідних запитів на модулі.
//     //await appLoader({expressApp: app});
//
//     // Починаємо "слухати" всі вхідні запити на визначеному порту (process.env.PORT - системний об'єкт, який визначається середовищем де запускається сервер)
//     app.listen(config.port, err => {
//         if (err) {
//             process.exit(1);
//             return;
//         }
//
//         console.log(`################################################
//  Server listening on port: ${process.env.PORT || 3000}
// ################################################`);
//     });
// }
//
// // Запускаємо щойно створену функцію
// startServer();
