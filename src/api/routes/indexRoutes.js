import {Router} from 'express';

const route = Router();

export default app => {
    app.use('/', route);

    route.get('/',
        async (request, responce, next) => {

            responce.render('layout');
        }
    );

    /**
     * Health Check endpoints
     * @TODO Explain why they are here
     */
    app.get('/status', (req, res) => {
        res
            .status(200)
            .json({
                status: true,
                message: "Server is up and running."
            })
            .end();
    });

}
