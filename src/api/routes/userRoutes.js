import {Router} from 'express';
import User from '../../db/models/userModel';

const route = Router();

const users = [
    {id: 1, name: 'Test user 1'},
    {id: 2, name: 'Test user 2'}
];


export default (app) => {
    app.use('/users', route);

    route.get('/',
        async (request, responce, next) => {

            const list = await User.find();

            return responce
                .json({
                    users:list,

                });
        }
    );

}
