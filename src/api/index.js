import { Router } from 'express';
import indexRoute from './routes/indexRoutes';
import userRoutes from './routes/userRoutes';

export default () => {
    const app = Router();

    indexRoute(app);
    userRoutes(app);

    return app;
}