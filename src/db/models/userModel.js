import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
    firstname: String,
    lastname: String,
    birthday: Date,
    title: String,
    has_pet: Boolean,
    saldo: Number,
    created: Date,
    updated: Date
});

export default mongoose.model('User', userSchema);