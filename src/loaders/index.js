import expressLoader from './express';
import mongoLoader from './mongodb';

export default async ({expressApp}) => {

   // Ініціалізація і підключення експресу
   await expressLoader({ app: expressApp });
    console.log('✌ Express was loaded');

    // З'єднання з Базою Даних
   // await mongoLoader();
    //console.log('✌ MongoDB was loaded');


}