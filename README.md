# fe10-sample

Тестовий сервер для демонстрації можливостей

На Heroku даний проект має наступне посилання: https://fe10-sample-server.herokuapp.com/

Серед можливостей даного сервера, необхідно відмітити:
- Головна сторінка (GET -> https://fe10-sample-server.herokuapp.com/ -> HTML)
- Статус сторінка (GET -> https://fe10-sample-server.herokuapp.com/status -> JSON)
- Список Користувачів (GET -> https://fe10-sample-server.herokuapp.com/users -> JSON)

## Сторонні сервіси, які задіяні в роботі даного сервера

- [CI/CD: Heroku](heroku.com)
- [Database: MongoDB Atlas](https://www.mongodb.com/cloud/atlas/lp/try2?utm_source=google&utm_campaign=gs_emea_ukraine_search_brand_atlas_desktop&utm_term=mongodb%20atlas&utm_medium=cpc_paid_search&utm_ad=e&gclid=CjwKCAjwvZv0BRA8EiwAD9T2VQUAsNX88_plurZAz-hw_gNdV4JC59Zjdo4wxr-cWh8NwleYk9AD2BoCLckQAvD_BwE)
- [Mockpus: Diagrams](https://app.diagrams.net/#G1ORZymSl-gn42esesLtjoNaL166hEGhGG)
- [Mongoose](https://mongoosejs.com/docs/api/schema.html#schema_Schema)

В ході роботи необхідно розділяти оточення розробки (development - ваш комп'ютер) та продашин(production - heroku у нашому випадку), тобто в залежності від оточення де запускається NodeJs, система повинна себе вести відповідно. Це досягається завдяки пакету `dotenv` (перекладається як "крапка енв"), який працює з файлами `.env`. Як правило, цей файл ігнорується і не відслідковуєтсья системою контроля версій.

Файл `.env.example` описує приклад структури, яку повинен мати `.env`.
Розробнику необхідно створити свій файл `.env` і внести свої налаштування. Це стосується рядка конекта до бази даних в першу чергу (беремо з сайту MongoDB Atlas).




